// Package telnet implements RFC 854.
//
// To do:
//
//     * support Sync/DM (data mark);
package telnet

import (
	"fmt"
	"io"
)

// Telnet command codes as per RFC 854.
const (
	CmdDM = iota + 242
	CmdBrk
	CmdIP
	CmdAO
	CmdAYT
	CmdEC
	CmdEL
	CmdGA
	CmdSB
	CmdWill
	CmdWont
	CmdDo
	CmdDont
)

type Cmd struct {
	Cmd   int    // command code as per RFC 854
	Offs  int    // offset of a data byte following the command (on Read)
	Data  []byte // data associated with the command, if any
	Trunc bool   // true if Data was truncated (on Read, due to Telnet.MaxSBParamLen)
}

// ToTelnet returns the Telnet command as Telnet data stream.  Do not
// pre-encode the SB command parameters as they will be encoded.
func (cmd Cmd) ToTelnet() ([]byte, error) {
	switch {
	case cmd.Cmd == CmdDM:
		panic("FIXME")

	case cmd.Cmd >= 241 && cmd.Cmd <= 249: // two-byte commands
		if len(cmd.Data) > 0 {
			return nil, fmt.Errorf("Command %d doesn't take any parameters (%d bytes given)", cmd.Cmd, len(cmd.Data))
		}
		return []byte{'\xff', byte(cmd.Cmd)}, nil

	case cmd.Cmd >= 251 && cmd.Cmd <= 254: // three-byte commands
		if len(cmd.Data) != 1 {
			return nil, fmt.Errorf("Command %d takes exactly one-byte parameter (%d given)", cmd.Cmd, len(cmd.Data))
		}
		return []byte{'\xff', byte(cmd.Cmd), cmd.Data[0]}, nil

	case cmd.Cmd == CmdSB:
		if len(cmd.Data) < 1 {
			return nil, fmt.Errorf("Command %d needs at least one-byte parameter (%d given)", cmd.Cmd, len(cmd.Data))
		}
		b := make([]byte, 0, 4+len(cmd.Data))
		b = append(b, '\xff', byte(cmd.Cmd), cmd.Data[0]) // head (SB)
		b = append(b, SBParamToTelnet(cmd.Data[1:])...)   // param
		return append(b, '\xff', '\xf0'), nil             // tail (SE)

	default:
		return nil, fmt.Errorf("Unknown command %d", cmd.Cmd)
	}
}

// SBParamToTelnet encodes the SB command parameter to satisfy the
// Telnet protocol, i.e., it doubles any '\xff' bytes.
func SBParamToTelnet(param []byte) []byte {
	out := make([]byte, 0, len(param))

	for _, b := range param {
		if b == '\xff' {
			out = append(out, '\xff')
		}
		out = append(out, b)
	}

	return out
}

// ToTelnet builds a Telnet byte stream out of the parameters which
// must be bytes, raw byte slices or Telnet command structs, and other
// types that support the fmt.Stringer interface.
func ToTelnet(params ...interface{}) ([]byte, error) {
	out := make([]byte, 0)

	for _, p := range params {
		switch pp := p.(type) {
		case byte:
			out = append(out, BytesToTelnet([]byte{pp})...)

		case []byte:
			out = append(out, BytesToTelnet(pp)...)

		case *Cmd:
			b, err := pp.ToTelnet()
			if err != nil {
				return nil, err
			}
			out = append(out, b...)

		case Cmd:
			b, err := pp.ToTelnet()
			if err != nil {
				return nil, err
			}
			out = append(out, b...)

		default:
			out = append(out, BytesToTelnet([]byte(fmt.Sprint(pp)))...)
		}
	}

	return out, nil
}

// BytesToTelnet encodes a raw stream from a byte slice into a Telnet
// data stream.  Note that all '\n' bytes are converted into the
// Telnet's '\r', '\n' sequence, there's no way to insert the single
// byte '\n' in the Telnet stream using this function; use append()
// directly on the byte slice to add the lone '\n' to the data stream.
func BytesToTelnet(bs []byte) []byte {
	out := make([]byte, 0, len(bs))

	for _, b := range bs {
		switch b {
		case '\n':
			out = append(out, '\r', '\n')
		case '\r':
			out = append(out, '\r', '\x00')
		case 255:
			out = append(out, '\xff', '\xff')
		default:
			out = append(out, b)
		}
	}

	return out
}

// Telnet is a ReadWriter that translates any data reads/writes
// from/to the Telnet protocol.
type Telnet struct {
	Cmds          []Cmd // commands from the last read
	MaxSBParamLen int   // option subneg param max len (> 0)
	r             io.Reader
	w             io.Writer
	state         int    // interpreter state at the end of the last read
	trunc         bool   // option subneg param truncation flag
	buf           []byte // incompletely interpreted tail of the last read
}

// New returns a new initialized Telnet structure.
func New(r io.Reader, w io.Writer) *Telnet {
	return &Telnet{MaxSBParamLen: 4096, r: r, w: w}
}

// Reset re-initializes the Telnet structure.  MaxSBParamLen is not
// changed.
func (t *Telnet) Reset() {
	t.Cmds = nil
	t.state = 0
	t.trunc = false
	t.buf = nil
}

// Read reads and interprets up to len(p) bytes into p.  Telnet.Cmds
// will be set to commands encountered.
func (t *Telnet) Read(p []byte) (int, error) {
	buf := make([]byte, len(p))
	n, err := t.r.Read(buf)
	nn := t.fromTelnet(buf[:n], p)

	return nn, err
}

func (t *Telnet) fromTelnet(ibuf []byte, obuf []byte) int {
	const (
		stateData = iota // must be 0 to be Telnet.state default
		stateCR
		stateCmd
		stateSBPrm
		stateSBPrmCmd
		stateSB   = CmdSB
		stateWill = CmdWill
		stateWont = CmdWont
		stateDo   = CmdDo
		stateDont = CmdDont
	)

	if len(ibuf) > len(obuf) {
		panic("Output buffer is not big enough")
	}

	t.Cmds = make([]Cmd, 0)

	i := 0
	out := func(b byte) {
		obuf[i] = b
		i++
	}

	for _, b := range ibuf {
		switch t.state {
		case stateData:
			switch {
			case b == 13: // CR
				t.state = stateCR

			case b == 255: // IAC (escape)
				t.state = stateCmd

			case b >= 7 && b <= 12: // BEL, BS, HT, LF, VT, FF
				out(b)

			case b <= 31 || b >= 128: // covers 0 (NUL)
				// "Take no action" by ignoring.

			default:
				out(b)
			}

		case stateCR:
			switch b {
			case 10: // LF
				out('\n')

			case 0: // NUL
				out('\r')

			default: // invalid, ignore
			}
			t.state = stateData

		case stateCmd:
			switch {
			case b == 242: // DM (data mark) Should we
				// skip data up to DM?  Requires
				// knowledge of the current mode
				// (normal/urgent).
				panic("FIXME")

			case b >= 243 && b <= 249: // BRK, IP, AO, AYT, EC, EL, GA
				t.Cmds = append(t.Cmds, Cmd{Cmd: int(b), Offs: i})
				t.state = stateData

			case b >= 250 && b <= 254: // SB, WILL, WONT, DO, DONT
				t.state = int(b)

			case b == 255: // IAC (interpret as command)
				out(255)
				t.state = stateData

			default: // NOP or invalid, ignore
				t.state = stateData
			}

		case stateSB:
			t.buf = make([]byte, 1)
			t.buf[0] = b // option code
			t.trunc = false
			t.state = stateSBPrm

		case stateSBPrm:
			switch b {
			case 255: // IAC (interpret as command)
				t.state = stateSBPrmCmd

			default:
				if len(t.buf) < t.MaxSBParamLen {
					t.buf = append(t.buf, b)
				} else {
					t.trunc = true
				}
			}

		case stateSBPrmCmd:
			switch b {
			case 240: // SE (end of subnegotiation parameters)
				t.Cmds = append(t.Cmds, Cmd{Cmd: CmdSB, Offs: i, Data: t.buf, Trunc: t.trunc})
				t.buf = nil
				t.state = stateData

			case 255: // IAC (interpret as command)
				if len(t.buf) < t.MaxSBParamLen {
					t.buf = append(t.buf, b)
				} else {
					t.trunc = true
				}
				t.state = stateSBPrm

			default: // invalid, ignore
				t.state = stateSBPrm
			}

		default:
			if t.state >= stateWill && t.state <= stateDont { // stateWill, stateWont, stateDo, stateDont
				t.Cmds = append(t.Cmds, Cmd{Cmd: t.state, Offs: i, Data: []byte{b}})
				t.state = stateData
			} else {
				panic("Invalid state, should never happen")
			}
		}
	}

	return i
}
