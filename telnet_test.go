package telnet

import (
	"bytes"
	"io"
	"runtime/debug"
	"testing"
)

func cmpCmds(cmd1, cmd2 *Cmd) bool {
	return (cmd1.Cmd == cmd2.Cmd &&
		cmd1.Offs == cmd2.Offs &&
		bytes.Compare(cmd1.Data, cmd2.Data) == 0 &&
		cmd1.Trunc == cmd2.Trunc)
}

func validateRead(t *testing.T, tn *Telnet, n int, p []byte, cmds []Cmd, err error) {
	gotP := make([]byte, n)
	gotN, gotErr := tn.Read(gotP)

	if err != gotErr {
		t.Errorf("Want %v, got %v\n%s", err, gotErr, string(debug.Stack()))
	}
	if 0 != bytes.Compare(p, gotP[:gotN]) {
		t.Errorf("Want %v, got %v\n%s", p, gotP[:gotN], string(debug.Stack()))
	}
	if len(cmds) != len(tn.Cmds) {
		t.Errorf("Want %d commands, got %d\n%s", len(cmds), len(tn.Cmds), string(debug.Stack()))
	}
	for i, cmd := range cmds {
		if !cmpCmds(&cmd, &tn.Cmds[i]) {
			t.Errorf("command #%d: Want %v, got %v\n%s",
				i, cmd, tn.Cmds[i], string(debug.Stack()))
		}
	}
}

func TestRead(t *testing.T) {
	b := bytes.NewBuffer(nil)
	tn := New(b, nil)

	// Empty stream.
	validateRead(t, tn, 256, []byte{}, []Cmd{}, io.EOF)

	// Data-only stream.
	b.Write([]byte("abcd"))
	validateRead(t, tn, 256, []byte("abcd"), []Cmd{}, nil)
	validateRead(t, tn, 256, []byte{}, []Cmd{}, io.EOF)

	// Data-only stream spanning reads.
	b.Write([]byte("abcdefg"))
	validateRead(t, tn, 4, []byte("abcd"), []Cmd{}, nil)
	validateRead(t, tn, 4, []byte("efg"), []Cmd{}, nil)
	validateRead(t, tn, 256, []byte{}, []Cmd{}, io.EOF)

	// Special chars.
	b.Write([]byte{
		'a', '\x00', 'b', '\n', 'c', '\r',
		'\n', 'd', '\r', '\x00', 'e', '\r',
		'f', '\a', '\b', '\t', '\v', '\f'})
	validateRead(t, tn, 6, []byte{'a', 'b', '\n', 'c'}, []Cmd{}, nil)
	validateRead(t, tn, 6, []byte{'\n', 'd', '\r', 'e'}, []Cmd{}, nil)
	validateRead(t, tn, 6, []byte{'\a', '\b', '\t', '\v', '\f'}, []Cmd{}, nil)
	validateRead(t, tn, 256, []byte{}, []Cmd{}, io.EOF)

	// Ignored chars.
	b.Write([]byte{byte('a')})
	for i := 1; i <= 6; i++ {
		b.Write([]byte{byte(i)})
	}
	b.Write([]byte{byte('b')})
	for i := 13; i <= 31; i++ {
		b.Write([]byte{byte(i)})
	}
	b.Write([]byte{'c'})
	for i := 128; i <= 254; i++ {
		b.Write([]byte{byte(i)})
	}
	b.Write([]byte{'d'})
	validateRead(t, tn, 8, []byte{'a', 'b'}, []Cmd{}, nil)
	validateRead(t, tn, 20, []byte{'c'}, []Cmd{}, nil)
	validateRead(t, tn, 64, []byte{}, []Cmd{}, nil)
	validateRead(t, tn, 64, []byte{'d'}, []Cmd{}, nil)
	validateRead(t, tn, 64, []byte{}, []Cmd{}, io.EOF)

	// Two-byte commands.
	b.Write([]byte{
		'a', '\xff', '\xf0', 'b', '\xff', '\xf1', 'c', '\xff',
		'\xf3', 'd', '\xff', '\xf4', '\xff', '\xf5', '\xff', '\xf6',
		'\xff', '\xf7', '\xff', '\xf8', 'e', '\xff', '\xf9', '\xff',
		'\xff'})
	validateRead(t, tn, 8, []byte{'a', 'b', 'c'}, []Cmd{}, nil)
	validateRead(t, tn, 8, []byte{'d'}, []Cmd{
		Cmd{Cmd: CmdBrk, Offs: 0, Data: nil, Trunc: false},
		Cmd{Cmd: CmdIP, Offs: 1, Data: nil, Trunc: false},
		Cmd{Cmd: CmdAO, Offs: 1, Data: nil, Trunc: false},
		Cmd{Cmd: CmdAYT, Offs: 1, Data: nil, Trunc: false},
	}, nil)
	validateRead(t, tn, 8, []byte{'e'}, []Cmd{
		Cmd{Cmd: CmdEC, Offs: 0, Data: nil, Trunc: false},
		Cmd{Cmd: CmdEL, Offs: 0, Data: nil, Trunc: false},
		Cmd{Cmd: CmdGA, Offs: 1, Data: nil, Trunc: false},
	}, nil)
	validateRead(t, tn, 8, []byte{'\xff'}, []Cmd{}, nil)
	validateRead(t, tn, 8, []byte{}, []Cmd{}, io.EOF)

	// Three-byte commands.
	b.Write([]byte{
		'\xff', '\xfb', '\x00', 'a', '\xff', '\xfc',
		'\x01', 'b', '\xff', '\xfd', '\x02', '\xff',
		'\xfe', '\x03', 'c'})
	validateRead(t, tn, 6, []byte{'a'}, []Cmd{
		Cmd{Cmd: CmdWill, Offs: 0, Data: []byte{'\x00'}, Trunc: false},
	}, nil)
	validateRead(t, tn, 6, []byte{'b'}, []Cmd{
		Cmd{Cmd: CmdWont, Offs: 0, Data: []byte{'\x01'}, Trunc: false},
		Cmd{Cmd: CmdDo, Offs: 1, Data: []byte{'\x02'}, Trunc: false},
	}, nil)
	validateRead(t, tn, 6, []byte{'c'}, []Cmd{
		Cmd{Cmd: CmdDont, Offs: 0, Data: []byte{'\x03'}, Trunc: false},
	}, nil)
	validateRead(t, tn, 6, []byte{}, []Cmd{}, io.EOF)

	// Option subnegotiation.
	b.Write([]byte{
		'a', '\xff', '\xf0', 'b', '\xff', '\xfa', '\x00', '\xff',
		'\xf0', 'c', '\xff', '\xfa', '\x01', '\x10', '\x11', '\xff',
		'\xff', '\x12', '\xff', '\x01', '\xff', '\xf0',
	})
	validateRead(t, tn, 8, []byte{'a', 'b'}, []Cmd{}, nil)
	validateRead(t, tn, 8, []byte{'c'}, []Cmd{
		Cmd{Cmd: CmdSB, Offs: 0, Data: []byte{'\x00'}, Trunc: false},
	}, nil)
	validateRead(t, tn, 8, []byte{}, []Cmd{
		Cmd{Cmd: CmdSB, Offs: 0, Data: []byte{'\x01', '\x10', '\x11', '\xff', '\x12'}, Trunc: false},
	}, nil)
	validateRead(t, tn, 8, []byte{}, []Cmd{}, io.EOF)

	// Option subnegotiation spanning several reads.
	b.Write([]byte{
		'a', '\xff', '\xfa', 'o',
		'b', 'c', '\xff', '\xff',
		'd', '\xff', '\x00', 'e',
		'f', 'g', 'h', '\xff',
		'\xf0', 'i'})
	validateRead(t, tn, 4, []byte{'a'}, []Cmd{}, nil)
	validateRead(t, tn, 4, []byte{}, []Cmd{}, nil)
	validateRead(t, tn, 4, []byte{}, []Cmd{}, nil)
	validateRead(t, tn, 4, []byte{}, []Cmd{}, nil)
	validateRead(t, tn, 4, []byte{'i'}, []Cmd{
		Cmd{Cmd: CmdSB, Offs: 0, Data: []byte{'o', 'b', 'c', '\xff', 'd', 'e', 'f', 'g', 'h'}, Trunc: false},
	}, nil)
	validateRead(t, tn, 4, []byte{}, []Cmd{}, io.EOF)

	// Option subnegotiation with truncated parameters.
	tn.MaxSBParamLen = 6
	b.Write([]byte{
		'\xff', '\xfa', '\x00', '\x01',
		'\x02', '\x03', '\x04', '\xff',
		'\xff', '\xfe', '\xfd', '\xff',
		'\xf0', 'a'})
	validateRead(t, tn, 4, []byte{}, []Cmd{}, nil)
	validateRead(t, tn, 4, []byte{}, []Cmd{}, nil)
	validateRead(t, tn, 4, []byte{}, []Cmd{}, nil)
	validateRead(t, tn, 4, []byte{'a'}, []Cmd{
		Cmd{Cmd: CmdSB, Offs: 0, Data: []byte{'\x00', '\x01', '\x02', '\x03', '\x04', '\xff'}, Trunc: true},
	}, nil)
	validateRead(t, tn, 4, []byte{}, []Cmd{}, io.EOF)
}

func TestBytesToTelnet(t *testing.T) {
	if 0 != bytes.Compare([]byte{},
		BytesToTelnet([]byte{})) {
		t.Error(string(debug.Stack()))
	}

	if 0 != bytes.Compare([]byte{'a', 'b', '\t', '\v'},
		BytesToTelnet([]byte{'a', 'b', '\t', '\v'})) {
		t.Error(string(debug.Stack()))
	}

	if 0 != bytes.Compare([]byte{'a', '\r', '\n', 'b', '\r', '\x00', '\xff', '\xff'},
		BytesToTelnet([]byte{'a', '\n', 'b', '\r', '\xff'})) {
		t.Error(string(debug.Stack()))
	}
}

func validateCmdToTelnet(t *testing.T, cmd *Cmd, bs []byte, err error) {
	gotBs, gotErr := cmd.ToTelnet()
	if err != gotErr {
		t.Errorf("Want %v, got %v\n%s", err, gotErr, string(debug.Stack()))
	}
	if 0 != bytes.Compare(bs, gotBs) {
		t.Errorf("Want %v, got %v\n%s", bs, gotBs, string(debug.Stack()))
	}
}

func TestCmdToTelnet(t *testing.T) {
	validateCmdToTelnet(t, &Cmd{Cmd: CmdBrk},
		[]byte{'\xff', '\xf3'},
		nil)

	bs, err := Cmd{Cmd: CmdBrk, Data: []byte{'a'}}.ToTelnet()
	if err == nil || bs != nil {
		t.Error(string(debug.Stack()))
	}

	validateCmdToTelnet(t, &Cmd{Cmd: CmdDont, Data: []byte{'\xff'}},
		[]byte{'\xff', '\xfe', '\xff'},
		nil)

	bs, err = Cmd{Cmd: CmdDont}.ToTelnet()
	if err == nil || bs != nil {
		t.Error(string(debug.Stack()))
	}

	bs, err = Cmd{Cmd: CmdDont, Data: []byte{'\xab', 'a'}}.ToTelnet()
	if err == nil || bs != nil {
		t.Error(string(debug.Stack()))
	}

	validateCmdToTelnet(t, &Cmd{Cmd: CmdSB, Data: []byte{'\xab', 'a', '\r', '\xff', 'c', '\n'}},
		[]byte{'\xff', '\xfa', '\xab', 'a', '\r', '\xff', '\xff', 'c', '\n', '\xff', '\xf0'},
		nil)

	validateCmdToTelnet(t, &Cmd{Cmd: CmdSB, Data: []byte{'\xff', 'a', '\xff'}},
		[]byte{'\xff', '\xfa', '\xff', 'a', '\xff', '\xff', '\xff', '\xf0'},
		nil)
}

func validateToTelnet(t *testing.T, bs []byte, err error, params ...interface{}) {
	gotBs, gotErr := ToTelnet(params...)
	if err != gotErr {
		t.Errorf("Want %v, got %v\n%s", err, gotErr, string(debug.Stack()))
	}
	if 0 != bytes.Compare(bs, gotBs) {
		t.Errorf("Want %v, got %v\n%s", bs, gotBs, string(debug.Stack()))
	}
}

func TestToTelnet(t *testing.T) {
	validateToTelnet(t, []byte{}, nil)

	validateToTelnet(t, []byte{'\xff', '\xfb', '\xd1'}, nil,
		Cmd{Cmd: CmdWill, Data: []byte{'\xd1'}})

	validateToTelnet(t, []byte{'\xff', '\xf6', 'a', '\r', '\n', 'b', '\xff', '\xff', '\r', '\x00', '\xff', '\xf8'}, nil,
		Cmd{Cmd: CmdAYT}, byte('a'), byte('\n'), []byte{'b', '\xff', '\r'}, &Cmd{Cmd: CmdEL})

	validateToTelnet(t, []byte{'a', 'b', 'c', '1', '2', '3'}, nil,
		"abc", 123)

	bs, err := ToTelnet(Cmd{Cmd: CmdDont, Data: []byte{'\xab', 'a'}})
	if err == nil || bs != nil {
		t.Error(string(debug.Stack()))
	}
}
